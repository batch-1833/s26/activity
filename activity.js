// 1.  import directive: require

// 2. http module

// 3. createServer() method

// 4. HTTP Status Codes

// 5. terminal

// 6. URL


// Install nodemon
// npm install -g nodemon
	// Installing this package will allow the server to automatically restart when files have been changed for update.

// Run the js file using this command:
	// nodemon fileName.js

const http = require("http");

const port = 3000;

http.createServer((request, response) => {
	// "request" is an object that is sent via the client (browser).
	// The "url" property refers to the url or the link in the browser.
	if(request.url == '/login'){
		response.writeHead(200, {"Content-Type": "text/plain"})
		response.end("You are in the login page");
	}
	 // Accessing the "homepage" route returns a message of "This is the homepage"
	else if(request.url == '/register'){
		response.writeHead(200, {"Content-Type": "text/plain"})
		response.end("page is not available.");
	}
	// All other routes will return a message of "Page not available"
	else{
		// Set a status code for the response - a 404 means Not Found
		response.writeHead(404, {"Content-Type": "text/plain"})
		response.end("Page is not available.");
	}
}).listen(port);

console.log(`Server now accessible at localhost: ${port}`);

// To check if we can access the routes:
 	// Browser > localhost:4000